<?php
function tentukan_nilai($number)
{
    if($number<=100 && $number >=85){
        $output = "Sangat Baik";
    }elseif($number<=84 && $number >=70){
        $output = "Baik";
    }elseif($number<=69 && $number >=45){
        $output = "Cukup";
    }elseif($number<=44 && $number >=0){
        $output = "Kurang";
    }
    return $output;
}

//TEST CASES
echo tentukan_nilai(98) . "<br>"; //Sangat Baik
echo tentukan_nilai(76). "<br>"; //Baik
echo tentukan_nilai(67). "<br>"; //Cukup
echo tentukan_nilai(43). "<br>"; //Kurang
?>
